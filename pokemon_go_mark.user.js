// ==UserScript==
// @name         IITC Plugin: Pokemon GO Mark
// @namespace    http://tampermonkey.net/
// @version      0.4.20181030.0845
// @description  Marque PokeStops e Ginasios (incluindo os Ex Raids) no Intel
// @author       @BPeralva
// @grant        none
// @match        https://*.ingress.com/intel*
// @match        https://intel.ingress.com/*
// @downloadURL  https://gitlab.com/peralva/publico/raw/master/pokemon_go_mark.user.js
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...

	var setup = function() {
		function adicionarIcone(S_GUID, N_Latitude, N_Longitude, N_Tipo, S_Nome) {
			if(typeof(N_Tipo) == "number") {
				var O_Icone;

				if(N_Tipo == 1) {
					O_Icone = O_PokeStopIcone;
					O_Icone.className = "";
				} else if(N_Tipo == 2) {
					O_Icone = O_GinasioIcone;
					O_Icone.className = "";
				} else if(N_Tipo == 3) {
					O_Icone = O_GinasioIcone;
					O_Icone.className = "ex";
				} else if(N_Tipo == 4) {
					O_Icone = O_NaoExisteIcone;
					O_Icone.className = "";
				}

				O_Icones[S_GUID] = window.L.marker([N_Latitude, N_Longitude], {
					title: S_Nome,
					icon: window.L.divIcon(O_Icone)
				});

				window.registerMarkerForOMS(O_Icones[S_GUID]);
				O_Icones[S_GUID].on("spiderfiedclick", function () {
					window.renderPortalDetails(S_GUID);
				});

				if(N_Tipo == 1) {
					O_Icones[S_GUID].addTo(O_PokeStopsLayer);
				} else if(N_Tipo == 2) {
					O_Icones[S_GUID].addTo(O_GinasiosLayer);
				} else if(N_Tipo == 3) {
					O_Icones[S_GUID].addTo(O_GinasiosExRaidLayer);
				} else if(N_Tipo == 4) {
					O_Icones[S_GUID].addTo(O_NaoExisteLayer);
				}
			}
		}
		function atualizarSelect(N_GUID, B_Aguarde) {
			if(!B_Aguarde) {
				if(true
					&& typeof(window.selectedPortal) == "string"
					&& window.selectedPortal == N_GUID
				) {
					if(typeof(O_Status[N_GUID]) == "number") {
						if(O_Status[N_GUID] == 1) {
							document.getElementById("pokemon_go_selecao_" + N_GUID).style.backgroundColor = "rgb(255, 127, 0)";
							document.getElementById("pokemon_go_selecao_" + N_GUID).style.color = "rgb(255, 255, 255)";
							document.getElementById("pokemon_go_selecao_" + N_GUID).title = "Enviando dados...";
						}
					} else {
						document.getElementById("pokemon_go_selecao_" + N_GUID).style.backgroundColor = "rgb(255, 255, 255)";
						document.getElementById("pokemon_go_selecao_" + N_GUID).style.color = "rgb(0, 0, 0)";
						document.getElementById("pokemon_go_selecao_" + N_GUID).title = "";

						if(typeof(O_Portais[N_GUID]) == "object") {
							document.getElementById("pokemon_go_selecao_" + N_GUID).value = O_Portais[N_GUID].pokemon_go;
						} else {
							document.getElementById("pokemon_go_selecao_" + N_GUID).value = "";
						}
					}
				}
			} else {
				document.getElementById("pokemon_go_selecao_" + N_GUID).style.backgroundColor = "rgb(255, 255, 255)";
				document.getElementById("pokemon_go_selecao_" + N_GUID).style.color = "rgb(0, 0, 0)";
				document.getElementById("pokemon_go_selecao_" + N_GUID).title = "";

				if(typeof(O_Portais[N_GUID]) == "object") {
					document.getElementById("pokemon_go_selecao_" + N_GUID).value = O_Portais[N_GUID].pokemon_go;
				} else {
					document.getElementById("pokemon_go_selecao_" + N_GUID).value = "";
				}
			}

		}
		function deletarIcone(S_GUID) {
			if(typeof(O_Icones[S_GUID]) == "object") {
				O_PokeStopsLayer.removeLayer(O_Icones[S_GUID]);
				O_GinasiosLayer.removeLayer(O_Icones[S_GUID]);
				O_GinasiosExRaidLayer.removeLayer(O_Icones[S_GUID]);
				O_NaoExisteLayer.removeLayer(O_Icones[S_GUID]);
				delete(O_Icones[S_GUID]);
			}
		}

		window.plugin.pokemon_go_mark = function() {};
		window.plugin.pokemon_go_mark.enviarDados = function(S_GUID, S_PokemonGO) {
			var O_Portal = window.portalDetail.get(S_GUID);

			if(typeof(O_Portal) == "object") {
				var O_Saida = {};
				O_Saida.guid = S_GUID;
				O_Saida.nome = O_Portal.title;
				O_Saida.latitude = O_Portal.latE6 / 1000000;
				O_Saida.longitude = O_Portal.lngE6 / 1000000;
				O_Saida.imagem = O_Portal.image;

				if(S_PokemonGO !== "") {
					O_Saida.pokemon_go = parseInt(S_PokemonGO, 10);
				}

				document.getElementsByName("pokemon_go_formulario")[0].metodo.value = "setPortal";
				document.getElementsByName("pokemon_go_formulario")[0].json.value = JSON.stringify(O_Saida);
				document.getElementsByName("pokemon_go_formulario")[0].submit();

				O_Status[S_GUID] = 1;

				atualizarSelect(S_GUID);
			} else {
				atualizarSelect(S_GUID, true);
				alert("Aguarde o t&eacute;rmino de carregamento do mapa.");
			}
		};

		window.$("head").append(`
			<style>
				.ex:after {
					content: "EX";
					position: absolute;
					font-weight: bold;
					color: #FFFFFF;
					text-shadow: 1px 1px #000000, -1px -1px #000000;
					top: 22px;
					left: 6px;
				}
			</style>
		`);

		window.$("body").append(`
			<form accept-charset='ISO-8859-1' name='pokemon_go_formulario' action='http://peralva.ddns.net:81/pgo/' method='post' target='pokemon_go_janela'>
				<input type='hidden' name='versao' value='` + GM_info.script.version + `' />
				<input type='hidden' name='url' value='` + GM_info.script.downloadURL + `' />
				<input type='hidden' name='metodo' />
				<input type='hidden' name='json' />
			</form>
		`);

		var O_Status = {};
		var O_Icones = {};

		var O_PokeStopsLayer = new window.L.LayerGroup();
		window.addLayerGroup("Pok&eacute;mon GO Mark - PokeStops", O_PokeStopsLayer);
		var O_GinasiosLayer = new window.L.LayerGroup();
		window.addLayerGroup("Pok&eacute;mon GO Mark - Gin&aacute;sios", O_GinasiosLayer);
		var O_GinasiosExRaidLayer = new window.L.LayerGroup();
		window.addLayerGroup("Pok&eacute;mon GO Mark - Gin&aacute;sios Ex Raid", O_GinasiosExRaidLayer);
		var O_NaoExisteLayer = new window.L.LayerGroup();
		window.addLayerGroup("Pok&eacute;mon GO Mark - N&atilde;o Existe", O_NaoExisteLayer);

		var O_PokeStopIcone = {
			html: "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAmCAYAAADJJcvsAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAHTElEQVRIx12VaYxeVRnHf89Z7n3foTNt6Uo7pbEFWSbIIpRAXGIiiSyRKA6gYoWGuCSiQZBKIIYPxOgnBK3ERK0YtNICHwjYWKUGArRoWZKiFktp6b7MtNOZeed97z3L44feaYjn0z3nOed5/v//s1xZuXLljLqup7rdbh7oH2jb0tgDeUd48PjWWEt9WZTwXQEfqR+Zn2Zvu2/gCpb6C1qSLb2q1+t0Oqm/v7+UW2+91eeUfSLoqN2fVtebU08nLmnpwNcM9iaLHzQYIvX+TNowIcfWHTE733qqvDfPj8tbRq2KCHL2LZazdFl7UC8qvxGeXOJpf13RWwLdQcHgaQGQSQR6OIoDFv+UwNo1/qZdo7LXD+jCjtxx7d19N5T3nN/PvNuUfLOjWJwIGCyKAopgyUQsnkRsbHl/IjydJKzdMPfu9+XFOyYelbEZN5JZmiQSpYfTFhYHQKQiESloIxgUJdBDELy0IPFB72h6Wt54uDuSTvo51WGj3claUAEUSwEoiQAIgpxGpJIhK3nMabXHSG8kjLqyXWhsBexspTzhcnXYSTWZJGiFIDg8giERCNJFsoUThXb3qHaP1pJrRdSqA0EVxKm0Fxppz0G7I5InjqjESYQsYIAshDHVsBfSfsi1mGwFNRmTBBdShTUOYwpiDhhnpFiYZN7cUnujmicP1VIdNPR2K/FgmxiiQA0WjBokepKtxSFirJwS1oihThWFLbHeyIxFCD7qyN9q9HBbTunkCL6LqGBTAYBNHiOQk0ayZrImStsiayJrJuaIIGLrUpIJqCjJBmwqELUkG1DJJBsw3hQkTdS5hxOPEYsznpArkka88Rh1GLUEPwUIJrsGTSa6HiY7NVkVAYxYMvlUFWtGxCAIWRUAFcVkB2RAUTl1LmpRSbiotXgpEDGEXJOatBemQEXpaiBaQC0ulmQTia5utHENxVqMIIiY02JnTc1eEAwIqCSMmg8hOIVKmjOTLUZVN4RcH5kWvLRtVDNJIzEHBINLLZIJZJPIJuBigcmOZAPZxGPBTz1rCmfuGu0dvGkqTjzhjD8mSCN2TSbhxCNqMNkRXQ9RQdRisjuaTVg7MnfHl3ct33SX3df3plv/7mOH2q5v48K+pa+03Bkp5nC2FdsnIqSucvKfQqgjNnuyyaMg6+qic/9rVz/yxKZrH9zV6Tvk7cUXXVLOaS+sdrvXwkObvncgysmXLzhzxeY+35+NMWfX3dg3+maAjh8x6tZlE3/w12t++MeN1921f2JgX/eln29Pz1zwLDI8PHwG0G3y2oo5uPfH/lM/c8OO3CWs6BwKd+77Q9bukfTEQJy55aerl+dFH3ysbZJXoAckoJTh4WEPeEAbZxVQhlyZbUc35xV7H1pmQzvt/upP9izZf4l1sWWawAKUNMtAM3CgaL4BgvfeXx7uGOh18yOd2Hl00cavtF0uWkBsAqbmbgHUDmg3xhpoTSMS6IbyREhm3nrEJL90XxbDpGaKhoFpWEwCpWvQVI13AQaAzvA594QrrrvK7Hlr5PC+v3T88S1jKZw5ll6/dE2vuZMbJwqoaTxPO7TvjW2fOlmN2o/O/Pil48fi432t/j+1j5315Lzx8365dM8nL61mBH9s7o6qoTb9rpDh4WEDtEOuZfuRLd0XvnhkYZXqbyZNq8TIYsYK9qwx9MYC2YcDmPx7n1uP3/mc33fN2i/0mewsUBlA61zp0JyrWk/fuHdlUl4w4h4AFp8KJpxeymITytUE98Kvr8+rznv/+jLZ6nTW2pP1WP3pwS9NhdRbFHNYEDWYwpY4ccQcmB5qPrQBTLL1vOA7S2efXKYvfuK5ScDZoaEh33Yzqm6cCK8eeP71M9sLts5uzTdW/EdETCt0I8ffDJhOiWDGjZh1x+fuvH/T51Y/+/2X7xl/Z/4buOCTHRoa8oAZ6R6QThwrfvP2w7vbRd/mc2ZdvA3R+WlKlkxs87kO9ctiuXfblY//6rHv3LB7yQfn66YLNzgXPEDbDg0NxaZ+WiIyNbNvVto++kp4ZudjBwdnL9u4JF74r94O+/wB9+8f/+72z+zqtEbqoqrCzPHB1FR2G+jK8PCw+1Cpx6amCgQ/Pj5F3nblVWKt+k9tfa1PWsKpCTvdIq3mXTbNpgdMNeLPANyiNRs6fufl4r1/wFnzI/vf88vO31dMNW3U1yCpm3c6Pc3zh5q2BUy9+9lvcfDEe53ls4ceUhPN3itfGp87MgjYBNj/67fkmk2rMVjgJNDaN/fPfs3Nr140Y2LBqv2/tfHIutsmfnbvsrdmvjNcNiimqSXAu0aTMxqdxoE8Wh3sXb3g87NmuFmrnfM3a4CBycFixT++ffdE/8Fxm4rYIHENzQkjImVDqysipbVWZhXzivnlkkrEpKQRRUk22JljSzuixhljrEjznz8lfGm89w7orV+/vrLWalmW/WXR1tsvvq9T2HJNCGFLKqe2Zk2/uOztVV2Lr4qiaJdlWQLVueeeWxljwv8AC8LA2Am9JKoAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMDktMjRUMTk6MjI6NDArMDI6MDB1NHxBAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTA5LTI0VDE5OjIyOjQwKzAyOjAwBGnE/QAAAABJRU5ErkJggg==' />",
			iconAnchor: [9, 38],
			iconSize: [18, 38]
		};
		var O_GinasioIcone = {
			html: "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAYAAAA/xX6fAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAHh0lEQVRIx7WVaWxU1xXH//e+zfPmeTyeGS8DxgbiFWywAbPZmEhpUtrmQ2ho2kjdSCoFqVUT+iUqUiRSqjRVm6Zf2khNJBKFQFGAkLCkMWFpABs8tgl4m7HH9hgv47E9423s8Zt5795+wHVts5RE7V86etJdzu+cc989l2CRCCEoKCiA1+tNkiSpwO12l2pa8gpKBatpGhNjY2MdweDAlwD8WVlZRl9fHx4kTdMQjUYFQRAe45xXkntMKm73ksdz8/J+kpnp3mK1ammSLMuKokCRZRiGoUci4YGuzs5LzS1N746PjV2VJMlMJBILQKIo4tiJT/D0zifzytZt2KNp2q56T92BOaDL5UI4PLJs/fqNr+QXrnrWZrNrnHOYzAQ4BwiBQClkWYaqqnA4UjE2Gg6fPXP6ba+37Q1d10f+XaGysnVobGxIy88v+GHRquI9y5Ytz/f7vZfPVf/jKQEAFEVBXNdzt1Zuf6ekdP0uxaLJJmPgICCUglABhFAwTmCYJqZjM0gYJpblLFdVi1p5uyegTE5OXszIyDCj0agFwFNbK6reXLdh8/POtEzXZHTCqK+rPRCNRmtFANB13Vm5bfubZRs2PUaoCM7Z4pOd/XIAAKUUpmHgem3thL/D++HU9NRBAGY0Gt1auW37i2vWlj3pcGWosZkZMNNEsL/3ZiAQOAMAIgBaWFj0wsZNFd92uNIWwAihYMxEPB4H5xyyJAMECAUHjLbWpostzbf+NDQ09DmA7LWlZa+Xl2/+UWHR6nQQithMDElJCkaGh5jf73v/96/tH3p5336IkiQVrVtf/rPcvFw6PyfGOHoC3UZHu7dpeHio0TRN3W5PLSKE2LxtLW93d3cdAUBXrFi5p3zjpp+vLV1fmJGRCdM0YZgmVDUJjDE0eq75uru6Try8b/+dnyk7J+d7q4tLcpxOBzhjAAEMw8TVK5cj1Z+d/W2n33/BYrFk2Ww2ORQKvQugB8Co2+3eUVq6bu+WisptjzySK4qiCM75f6pDKQJdXby1pekwY2zu7ohZWdnfzMrKovYUGzjnIITAU+eJf3rmk1dHR0dv7fzurr9VVFSszc5eJtR76vtPnDj+R7+/4+zq4jWv//inu1c5nU5wzjCPBTIbdGtrS6Cjo/3o3OEDEJ1OV2GaywV7ig0AMD09jRuN9Q2BQODSM8/84L3dzz1f6nZnwuV0oLi4JEcUpdfeeusvnZFI+CznbFVKiu1OZeaJUoq2Ni8aG+uPmabZuWBOVVWb3W5DSkoyrKqKrs4utLf7LjscjvxtVVUlK1bkQJJETExOglKCqu1Vyfn5+Tt8Pu/VkeFhI8WWjORkbc5syRqSFAX19Z6gt631EIAF0Yicc8Y5aG9vPy6cPz9aXf3ZkfZ231+dTuejmqYJVlUFYyY4AEoIkpM1KEpSsj4zoxNwZrWqME1zQXYtLa2orbn6cSKRaFnc6sTh4aHIRx+dtDc01J27fv3aG9NTU18AsIfD4baamtoRe6rLZbfbQCiBQAV46jyspydwbcmSpbkcghTo6QNbVNLq6nPh5uam9wCYi4HE6XL9WRCEpqFQ6O8AkrKzl++2asm7+noD+9IzMstXF5e9kupMtyZZkmAmdHbzhue4p+7a/vJNW98pKdu0RZIk8PkemYm6mksffHmj4TkA8buAAGQANC0t/TsZmUv3Sqp9CxFEGo+G6zp8zS9ZLGp6RsaSJ6ggWEZHw9cHgwNXcvMK9qa4snZHZww6/yoIAoVV4pOd3ps7R8fGzt/r9SAAJLfb/Wr2yqIXE1xSZ2a7itWigOnRwFg4dCQSGakzTSOeYnfkOZzp30/SHJundIPMLyXngEWRMD0W/NjX1vwsgNi9gCIAgzHuJYQQUZRgFYW5rqlojuVLbam/dmcbBuecUUGUOREQT5hQLeLCyAkFMWOx20PBg5SQGOMc9wPyUGjwqMWirsleWfgSlVVhLvfZPaJMxDtBcIADkny3I4EAg719NeFw+AIeIPFOdEQPBLoOEEKkrJzcF5K0FEUQRQiCAELILJiD38cJIQTTE5F4KNh/UBDopGmy+wKFObIo6pFI+J/xmelBkZI8UZBcgiBCEkXIsgxFkaHIMiRJgjzPFFmCSCn6e/yeri7/bzjnMTxAc0DGGAghxi9++av64x8e/jShT/fHZ6bF+ExMiesxmtBjRLNqgmpVIckS5DmTMTkaNttabv0OnF1OJIwH8UDuNagoCh5/4ls4feqklQBLVKuaQQkt2Fzx6B8KVpekzl9rJAw01F295bleswNAEP9F9F6Duq7j9KmTADBFKO1gjF+ZjEYPhYL9dYQzWFULrBYLNKuKyfEI6+3pPvQwsPsC54sxhlgsBgB6cLD/9Gg4zFSLBarVAlEQ0BPo6gyFBo89DOyhgPMVHhn5fKD/dpAQQFNVREaGeXdnx1HOeeD/AmSMdfb39V6ZGBuHIFB0tLf1BYMDh4H73pi7JD7swlklBgcHTvX13n7aMBJiu893gjHm+yoOvioQ4+PjX9zu6Q6EBoP2gYG+97Hogf2fAwH0+/2+i4RQKZFI3Pwa+7+WvgGg6uts/BfOXWqYHcUivAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0wOS0yNFQxOTo1MDozNyswMjowMJeYjmgAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMDktMjRUMTk6NTA6MzcrMDI6MDDmxTbUAAAAAElFTkSuQmCC' />",
			iconAnchor: [14, 30],
			iconSize: [28, 30]
		};
		var O_NaoExisteIcone = {
			html: "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAQAAAC1QeVaAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAADsMAAA7DAcdvqGQAAABOSURBVBjTzdAxDkBAEIbRh0o2SidxRycQLrsyCgqStVr/lG+qD+it4nar3rVkflAIswRDgU4eWOQiZksjvK7lX9gZTcWX3fYRoZqvEv4AyuxG3lbx3QAAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMTAtMDlUMjM6MDA6MDMrMDI6MDC8YzYiAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTEwLTA5VDIzOjAwOjAzKzAyOjAwzT6OngAAAABJRU5ErkJggg==' />",
			iconAnchor: [7, 7],
			iconSize: [14, 14]
		};

		var O_Portais;

		if(typeof(localStorage.pokemon_go_mark) == "string") {
			try{
				O_Portais = JSON.parse(localStorage.pokemon_go_mark);

				for(var S_GUID in O_Portais) {
					adicionarIcone(
						S_GUID,
						O_Portais[S_GUID].latitude,
						O_Portais[S_GUID].longitude,
						O_Portais[S_GUID].pokemon_go,
						O_Portais[S_GUID].nome
					);
				}
			} catch(O_Erro) {
				O_Portais = {};
			}
		} else {
			O_Portais = {};
		}

		window.addEventListener("message", function(O_Evento) {
			if(O_Evento.data.metodo == "setPortal") {
				delete(O_Status[O_Evento.data.guid]);

				if(typeof(O_Evento.data.pokemon_go) == "number") {
					if(typeof(O_Portais[O_Evento.data.guid]) != "object") {
						O_Portais[O_Evento.data.guid] = {};
					}

					O_Portais[O_Evento.data.guid].nome = O_Evento.data.title;
					O_Portais[O_Evento.data.guid].latitude = O_Evento.data.latitude;
					O_Portais[O_Evento.data.guid].longitude = O_Evento.data.longitude;
					O_Portais[O_Evento.data.guid].pokemon_go = O_Evento.data.pokemon_go;
				} else {
					delete(O_Portais[O_Evento.data.guid]);
				}

				atualizarSelect(O_Evento.data.guid);

				deletarIcone(O_Evento.data.guid);
				adicionarIcone(
					O_Evento.data.guid,
					O_Evento.data.latitude,
					O_Evento.data.longitude,
					O_Evento.data.pokemon_go,
					O_Evento.data.nome
				);
			} else if(O_Evento.data.metodo == "getPortais") {
				for(var N_Cont = 0; N_Cont < O_Evento.data.portais.length; N_Cont++) {
					if(typeof(O_Evento.data.portais[N_Cont].pokemon_go) == "number") {
						if(typeof(O_Portais[O_Evento.data.portais[N_Cont].guid]) != "object") {
							O_Portais[O_Evento.data.portais[N_Cont].guid] = {};
						}

						O_Portais[O_Evento.data.portais[N_Cont].guid].nome = O_Evento.data.portais[N_Cont].nome;
						O_Portais[O_Evento.data.portais[N_Cont].guid].latitude = O_Evento.data.portais[N_Cont].latitude;
						O_Portais[O_Evento.data.portais[N_Cont].guid].longitude = O_Evento.data.portais[N_Cont].longitude;
						O_Portais[O_Evento.data.portais[N_Cont].guid].pokemon_go = O_Evento.data.portais[N_Cont].pokemon_go;
					} else {
						delete(O_Portais[O_Evento.data.portais[N_Cont].guid]);
					}

					deletarIcone(O_Evento.data.portais[N_Cont].guid);
					adicionarIcone(
						O_Evento.data.portais[N_Cont].guid,
						O_Evento.data.portais[N_Cont].latitude,
						O_Evento.data.portais[N_Cont].longitude,
						O_Evento.data.portais[N_Cont].pokemon_go,
						O_Evento.data.portais[N_Cont].nome
					);

					delete(O_Status[O_Evento.data.portais[N_Cont].guid]);
					atualizarSelect(O_Evento.data.portais[N_Cont].guid);
				}
			}

			localStorage.pokemon_go_mark = JSON.stringify(O_Portais);
		}, false);
		window.addHook("mapDataRefreshEnd", function() {
			var O_Saida = [];

			window.$.each(window.portals, function(i, portal) {
				O_Saida.push({});
				O_Saida[O_Saida.length - 1].guid = portal.options.guid;

				if(typeof(portal.options.data.title) == "string") {
					O_Saida[O_Saida.length - 1].nome = portal.options.data.title;
					O_Saida[O_Saida.length - 1].latitude = portal.options.data.latE6 / 1000000;
					O_Saida[O_Saida.length - 1].longitude = portal.options.data.lngE6 / 1000000;
					O_Saida[O_Saida.length - 1].imagem = portal.options.data.image;
				}
			});

			document.getElementsByName("pokemon_go_formulario")[0].metodo.value = "getPortais";
			document.getElementsByName("pokemon_go_formulario")[0].json.value = JSON.stringify(O_Saida);
			document.getElementsByName("pokemon_go_formulario")[0].submit();
		});
		window.addHook("portalDetailsUpdated", function() {
			window.$('#portaldetails > .imgpreview').after(`
				<center>
					Pok&eacute;mon GO Mark: <select id='pokemon_go_selecao_` + window.selectedPortal + `' onchange='window.plugin.pokemon_go_mark.enviarDados(window.selectedPortal, this.value);'>
						<option />
						<option value=1>PokeStop</option>
						<option value=2>Gin&aacute;sio</option>
						<option value=3>Gin&aacute;sio Ex Raid</option>
						<option value=4>N&atilde;o Existe</option>
					</select>
				</center>
			`);

			atualizarSelect(window.selectedPortal);
		});
	};

	setup.info = GM_info;
//	setup.info.buildName = GM_info.script.name;

	if(typeof(window.bootPlugins) != "object") {
		window.bootPlugins = [];
	}

	window.bootPlugins.push(setup);
})();